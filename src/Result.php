<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/21 17:01,
 * @LastEditTime: 2022/10/21 17:01
 */
declare(strict_types=1);

namespace Zhen\HyperfRocketMQ;


class Result
{
    /**
     * Acknowledge the message.
     */
    public const ACK = 'ack';

    /**
     * Unacknowledged the message.
     */
    public const NACK = 'nack';

    /**
     * Reject the message and drop it.
     */
    public const DROP = 'drop';
}