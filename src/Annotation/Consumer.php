<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/5/11 22:40,
 * @LastEditTime: 2022/5/11 22:40
 */
declare(strict_types=1);

namespace Zhen\HyperfRocketMQ\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;
use Zhen\HyperfRocketMQ\Constants\MqConstant;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
#[Attribute(Attribute::TARGET_CLASS)]
class Consumer extends AbstractAnnotation
{
    public string $name = 'Consumer';

    /**
     * 驱动.
     */
    public string $poolName = 'default';

    public string $topic = '';

    public string $groupId;

    /**
     * filter tag for consumer. If not empty, only consume the message which's messageTag is equal to it.
     */
    public string $messageTag;

    /**
     * consume how many messages once, 1~16.
     */
    public int $numOfMessage = 1;

    /**
     * if > 0, means the time(second) the request holden at server if there is no message to consume.
     * If <= 0, means the server will response back if there is no message to consume.
     * It's value should be 1~30.
     */
    public int $waitSeconds = 3;

    /**
     * 进程数量.
     */
    public int $processNums = 1;

    /**
     * 是否初始化时启动.
     */
    public bool $enable = true;

    /**
     * 进程最大消费数.
     */
    public int $maxConsumption = 0;

    /**
     * 是否开启协程并发消费.
     */
    public bool $openCoroutine = false;

    /**
     * 是否添加 env 后缀（对 topic、groupId、messageTag 起效）
     * 主要为了区分多个环境共用一个实例
     * @var bool
     */
    public bool $addEnvExt = false;

    /**
     * 日志类型
     * @var int
     */
    public int $logType = MqConstant::LOG_TYPE_FILE;
}
